﻿namespace Delivery.BL
{
    public interface IDeliveryCalculator
    {
        decimal CalculateParcelCost(Parcel item);
    }
}

﻿namespace Delivery.BL.ParcelRules
{
    public class LargeParcelRule : IParcelRule
    {
        public decimal CalculateParcelCost(Parcel item)
        {
            return (decimal)0.03 * item.Volume;
        }
    }
}
